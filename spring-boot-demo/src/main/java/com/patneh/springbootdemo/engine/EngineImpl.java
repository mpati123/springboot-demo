package com.patneh.springbootdemo.engine;

public class EngineImpl implements Engine{
    private final EngineType type;
    private final String name;
    private final int horsepower;
    private final String transmission;
    private final int cc;

    public EngineImpl(EngineType type, String name, int horsepower, String transmition, int cc) {
        this.type = type;
        this.name = name;
        this.horsepower = horsepower;
        this.transmission = transmition;
        this.cc = cc;
    }


    @Override
    public EngineType getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getHorsepower() {
        return horsepower;
    }

    @Override
    public String getTransmission() {
        return transmission;
    }

    @Override
    public int getCC() {
        return cc;
    }
}
