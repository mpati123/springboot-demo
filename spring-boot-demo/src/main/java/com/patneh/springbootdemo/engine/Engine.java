package com.patneh.springbootdemo.engine;

public interface Engine {

    EngineType getType();

    String getName();

    int getHorsepower();

    String getTransmission();

    int getCC();

}
