package com.patneh.springbootdemo.engine;

public enum EngineType {
    DIESEL,
    PETROL
}
