package com.patneh.springbootdemo.creator;

import com.patneh.springbootdemo.car.Car;

public interface CarOfferCreator {
    String createCarOffer(Car car);
}
