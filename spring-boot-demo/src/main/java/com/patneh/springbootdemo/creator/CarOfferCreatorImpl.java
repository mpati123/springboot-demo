package com.patneh.springbootdemo.creator;

import com.patneh.springbootdemo.car.Car;
import com.patneh.springbootdemo.engine.Engine;
import com.patneh.springbootdemo.engine.EngineType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CarOfferCreatorImpl implements CarOfferCreator{

    private static final String EMPTY_VALUE = "-";
    private final String offerHeader;
    private final String offerFooter;

    public CarOfferCreatorImpl(
            @Value("${offer.header:#(null)}") String offerHeader,
            @Value("${offer.footer:#(null)}") String offerFooter) {
        this.offerHeader = offerHeader;
        this.offerFooter = offerFooter;
    }

    @Override
    public String createCarOffer(Car car) {

        StringBuilder stringBuilder = new StringBuilder();

        if(offerHeader!=null){
            stringBuilder.append("\n");
            stringBuilder.append(offerHeader);
        }

        stringBuilder.append("\nNazwa: ");
        stringBuilder.append(car.getName());

        stringBuilder.append("\nKolor: ");
        stringBuilder.append(car.getColor());

        stringBuilder.append("\nKoła: ");
        stringBuilder.append(car.getWheelsSize());

        stringBuilder.append("\nSilnik: ");
        stringBuilder.append(createEngineDescription(car.getEngine()));

        if(offerFooter!=null){
            stringBuilder.append("\n");
            stringBuilder.append(offerFooter);
        }

        return stringBuilder.toString();
    }

    private String createEngineDescription(Engine engine){
        if(engine==null){return EMPTY_VALUE;}

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("\n    Typ: ");
        stringBuilder.append(mapEngineType(engine.getType()));

        stringBuilder.append("\n    Nazwa: ");
        stringBuilder.append(engine.getName());

        stringBuilder.append("\n    Moc:");
        stringBuilder.append(engine.getHorsepower());

        stringBuilder.append("\n    Skrzynia Biegów:");
        stringBuilder.append(engine.getTransmission());

        stringBuilder.append("\n    Pojemność:");
        stringBuilder.append(engine.getCC());

        return stringBuilder.toString();
    }

    private String mapEngineType(EngineType type){
        if(type==null){return EMPTY_VALUE;}

        switch (type){
            case DIESEL: return "Diesel";
            case PETROL: return "Petrol";
        }
        return EMPTY_VALUE;
    }
}
