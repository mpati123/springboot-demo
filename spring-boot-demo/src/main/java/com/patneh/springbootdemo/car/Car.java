package com.patneh.springbootdemo.car;

import com.patneh.springbootdemo.engine.Engine;

public interface Car {

    String getName();

    Engine getEngine();

    void setColor(String color);

    String getColor();

    void setWheelsSize(int wheelsSize);

    int getWheelsSize();
}
