package com.patneh.springbootdemo;

import com.patneh.springbootdemo.car.Car;
import com.patneh.springbootdemo.engine.Engine;
import com.patneh.springbootdemo.service.CarOfferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringBootDemoApplication implements CommandLineRunner {

	private static final Logger LOG = LoggerFactory.getLogger(SpringBootDemoApplication.class);

	@Autowired
	ApplicationContext applicationContext;

	@Autowired
	CarOfferService carOfferService;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Engine engine1 = applicationContext.getBean("enginePetrol20", Engine.class);
		Engine engine2 = applicationContext.getBean("enginePetrol20", Engine.class);
		Car car1 = applicationContext.getBean("mazdaCx5Petrol25", Car.class);
		Car car2 = applicationContext.getBean("mazdaCx5Petrol25", Car.class);
		LOG.info("engine1==engine2 = {}", engine1==engine2);
		LOG.info("car1==car2 = {}", car1==car2);

		prepareAndSendOffer("mazda6Petrol20", "Bialy", 17, "kamil@gmail.com");
		prepareAndSendOffer("mazda6Petrol25", "Czerwony", 19, "weronika@gmail.com");
		prepareAndSendOffer("mazda6Diesel22", "Czarny", 18, "jarek@gmail.com");
		prepareAndSendOffer("mazdaCx5Petrol20", "Szary", 18, "filip@gmail.com");
		prepareAndSendOffer("mazdaCx5Petrol25", "Niebieski", 19, "ania@gmail.com");
		prepareAndSendOffer("mazdaCx5Diesel22", "Brazowy", 18, "piotr@gmail.com");
	}

	private void prepareAndSendOffer(String carBeanName, String carColor, int carWheelsSize, String clientEmail){
		Car car = applicationContext.getBean(carBeanName, Car.class);

		car.setColor(carColor);
		car.setWheelsSize(carWheelsSize);

		carOfferService.prepareAndSendOffer(car, clientEmail);
	}
}
