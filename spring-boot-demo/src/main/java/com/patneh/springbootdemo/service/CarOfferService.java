package com.patneh.springbootdemo.service;

import com.patneh.springbootdemo.car.Car;

public interface CarOfferService {
    void prepareAndSendOffer(Car car, String email);
}
