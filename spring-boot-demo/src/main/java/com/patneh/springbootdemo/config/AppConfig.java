package com.patneh.springbootdemo.config;

import com.patneh.springbootdemo.car.Car;
import com.patneh.springbootdemo.car.CarImpl;
import com.patneh.springbootdemo.engine.Engine;
import com.patneh.springbootdemo.engine.EngineImpl;
import com.patneh.springbootdemo.engine.EngineType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class AppConfig {

    private final String enginePetrol20Name;
    private final String enginePetrol25Name;
    private final String engineDiesel22Name;
    private final String carMazda6Name;
    private final String carMazdaCX5Name;

    @Autowired
    public AppConfig(
            @Value("${engine.petrol.20:}") String enginePetrol20Name,
            @Value("${engine.petrol.25:}") String enginePetrol25Name,
            @Value("${engine.diesel.22:}") String engineDiesel22Name,
            @Value("${car.mazda.6:}") String carMazda6Name,
            @Value("${car.mazda.cx5}") String carMazdaCX5Name) {
        this.enginePetrol20Name = enginePetrol20Name;
        this.enginePetrol25Name = enginePetrol25Name;
        this.engineDiesel22Name = engineDiesel22Name;
        this.carMazda6Name = carMazda6Name;
        this.carMazdaCX5Name = carMazdaCX5Name;
    }

    @Bean("enginePetrol20")
    public Engine enginePetrol20(){
        return new EngineImpl(EngineType.PETROL, enginePetrol20Name, 165, "6MT", 1988);
    }

    @Bean("enginePetrol25")
    public Engine enginePetrol25(){
        return new EngineImpl(EngineType.PETROL, enginePetrol25Name, 194, "6AT", 2488);
    }

    @Bean("engineDiesel22")
    public Engine engineDiesel22(){
        return new EngineImpl(EngineType.DIESEL, engineDiesel22Name, 184, "6AT", 2191);
    }

    @Bean("mazda6Petrol20")
    @Scope("prototype")
    public Car mazda6Petrol20(){
        return new CarImpl(carMazda6Name, enginePetrol20());
    }

    @Bean("mazda6Petrol25")
    @Scope("prototype")
    public Car mazda6Petrol25(){
        return new CarImpl(carMazda6Name, enginePetrol25());
    }

    @Bean("mazda6Diesel22")
    @Scope("prototype")
    public Car mazda6Diesel22(){
        return new CarImpl(carMazda6Name, engineDiesel22());
    }

    @Bean("mazdaCx5Petrol20")
    @Scope("prototype")
    public Car mazdaCx5Petrol20(){
        return new CarImpl(carMazdaCX5Name, enginePetrol20());
    }

    @Bean("mazdaCx5Petrol25")
    @Scope("prototype")
    public Car mazdaCx5Petrol25(){
        return new CarImpl(carMazdaCX5Name, enginePetrol25());
    }

    @Bean("mazdaCx5Diesel22")
    @Scope("prototype")
    public Car mazdaCx5Diesel22(){
        return new CarImpl(carMazdaCX5Name, engineDiesel22());
    }


}
